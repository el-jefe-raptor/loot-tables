import boto3

dynamodb = boto3.client('dynamodb',region_name='us-east-1', endpoint_url='http://127.0.0.1:8000/')
dynamodb.create_table(
    AttributeDefinitions=[
        {'AttributeName': 'id', 'AttributeType': 'S'}
    ],
    TableName="loottables-dev",
    KeySchema=[{'AttributeName': 'id', 'KeyType': 'HASH'}],
    ProvisionedThroughput={
        'ReadCapacityUnits': 123,
        'WriteCapacityUnits': 123,
})

resource = boto3.resource('dynamodb', region_name='us-east-1', endpoint_url='http://127.0.0.1:8000/')
table = dynamodb.Table('loottables-dev')
table.put_item(Item={'id': 'value'})
table.put_item(Item={'id': 'value2'})