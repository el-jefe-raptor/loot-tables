import datetime
import json
import uuid

import pytest

from loot_tables.serializers import loottable_serlializer as srs
from loot_tables.domain.loottable import LootTable


def test_serialize_domain_loottable():
    user_id = uuid.uuid4()
    id = uuid.uuid4()
    tags = ["Loot", "Test"]
    entries = ["1d10 gold", "a gem"]
    name = "test table"
    public = True

    loottable = LootTable(id, userId=user_id, name=name, tags=tags, entries=entries, public=public)

    expected_json = """
      {{
        "id": "{}",
        "userId": "{}",
        "name": "test table",
        "tags": ["Loot", "Test"],
        "entries": ["1d10 gold", "a gem"],
        "public": true
      }}
    """.format(str(id), str(user_id))

    json_loottable = json.dumps(loottable, cls=srs.LootTableEncoder)

    assert json.loads(json_loottable) == json.loads(expected_json)


def test_serialize_wrong_input():
    with pytest.raises(TypeError):
        json.dumps(datetime.datetime.now(), cls=srs.LootTableEncoder)