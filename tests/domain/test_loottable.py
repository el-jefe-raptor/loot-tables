import uuid
from loot_tables.domain.loottable import LootTable


def test_loottable_model_init():
    user_id = uuid.uuid4()
    id = uuid.uuid4()
    tags = ["Loot", "Test"]
    entries = ["1d10 gold", "a gem"]
    name = "test table"
    public = True

    loottable = LootTable(id, userId=user_id, name=name, tags=tags, entries=entries, public=public)

    assert loottable.Id == id
    assert loottable.UserId == user_id
    assert loottable.Name == name
    assert loottable.Tags == tags
    assert loottable.Entries == entries
    assert loottable.Public == public


def test_loottable_model_from_dict():
    user_id = uuid.uuid4()
    id = uuid.uuid4()
    tags = ["Loot", "Test"]
    entries = ["1d10 gold", "a gem"]
    name = "test table"
    public = True

    loottable = LootTable.from_dict(
        {
            'id': id,
            'userId': user_id,
            'tags': tags,
            'entries': entries,
            'name': name,
            'public': public
        }
    )

    assert loottable.Id == id
    assert loottable.UserId == user_id
    assert loottable.Name == name
    assert loottable.Tags == tags
    assert loottable.Entries == entries
    assert loottable.Public == public


def test_loottable_model_public_default():
    user_id = uuid.uuid4()
    id = uuid.uuid4()
    tags = ["Loot", "Test"]
    entries = ["1d10 gold", "a gem"]
    name = "test table"

    loottable = LootTable(id, userId=user_id, name=name, tags=tags, entries=entries)

    assert loottable.Id == id
    assert loottable.UserId == user_id
    assert loottable.Name == name
    assert loottable.Tags == tags
    assert loottable.Entries == entries
    assert loottable.Public == True    


def test_loottable_model_to_dict():
    loottable_dict = {
            'id': uuid.uuid4(),
            'userId': uuid.uuid4(),
            'tags': ["Loot", "Test"],
            'entries': ["1d10 gold", "a gem"],
            'name': "test table",
            'public': True
        }

    loottable = LootTable.from_dict(loottable_dict)

    assert loottable.to_dict() == loottable_dict


def test_loottable_model_comparison():
    loottable_dict = {
            'id': uuid.uuid4(),
            'userId': uuid.uuid4(),
            'tags': ["Loot", "Test"],
            'entries': ["1d10 gold", "a gem"],
            'name': "test table",
            'public': True
        }

    loottable = LootTable.from_dict(loottable_dict)
    other_loottable = LootTable.from_dict(loottable_dict)

    assert loottable == other_loottable
