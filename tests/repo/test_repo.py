import pytest

from loot_tables.repo.repo import LootTableRepo


def test_repo_list_not_implemented():
    with pytest.raises(NotImplementedError):
        repo = LootTableRepo()
        repo.list()