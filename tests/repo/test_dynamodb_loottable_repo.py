import pytest

import boto3
import botocore

from moto.dynamodb2 import mock_dynamodb2
from loot_tables.repo.dynamodb_repo import DynamoDbLootTableRepo


@pytest.fixture
def dynamodb_client():
    with mock_dynamodb2():
        dynamodb = boto3.client('dynamodb', 'us-east-1', endpoint_url="http://127.0.0.1:8000/")
        dynamodb.create_table(
            AttributeDefinitions=[
                {'AttributeName': 'id', 'AttributeType': 'S'}
            ],
            TableName="LootTable",
            KeySchema=[{'AttributeName': 'id', 'KeyType': 'HASH'}],
            ProvisionedThroughput={
                'ReadCapacityUnits': 123,
                'WriteCapacityUnits': 123,
            })

        resource = boto3.resource('dynamodb', region_name='us-east-1')
        table = resource.Table('LootTable')
        table.put_item(Item={'id': 'value'})
        table.put_item(Item={'id': 'value2'})
        yield resource


def test_dynamodb_repo_missing_client_error():
    with pytest.raises(Exception):
        DynamoDbLootTableRepo()


def test_dynamodb_repo_inits(dynamodb_client):
    client = dynamodb_client
    repo = DynamoDbLootTableRepo(client=client, table="LootTable")

    assert repo.client == client


def test_dynamodb_repo_missing_table_name(dynamodb_client):
    with pytest.raises(Exception):
        client = dynamodb_client
        DynamoDbLootTableRepo(client=client)


def test_dynamodb_repo_lists(dynamodb_client):
    client = dynamodb_client
    repo = DynamoDbLootTableRepo(client=client, table="LootTable")

    tables = repo.list()

    if 'Items' in tables:
        tables = tables['Items']

    assert tables[0]['id'] == 'value'
    assert tables[1]['id'] == 'value2'


def test_dynamodb_repo_lists_with_filters(dynamodb_client):
    client = dynamodb_client
    repo = DynamoDbLootTableRepo(client=client, table="LootTable")

    tables = repo.list(filters={'id': 'value2'})

    if 'Items' in tables:
        tables = tables['Items']

    assert tables[0]['id'] == 'value2'
