import uuid

import pytest
from unittest import mock

from loot_tables.domain.loottable import LootTable
from loot_tables.shared import response_object as res
from loot_tables.use_cases import request_objects as req, loottable_use_cases as uc


@pytest.fixture
def domain_loottables():
    loottable_1 = LootTable(
        uuid.uuid4(),
        userId=uuid.uuid4(),
        name="Loot Table 1",
        tags=["Loot", "Test"],
        entries=["1 thingy"],
        public=True
    )


    loottable_2 = LootTable(
        uuid.uuid4(),
        userId=uuid.uuid4(),
        name="Loot Table 2",
        tags=["Loot", "Test"],
        entries=["1 thingy"],
        public=True
    )


    loottable_3 = LootTable(
        uuid.uuid4(),
        userId=uuid.uuid4(),
        name="Loot Table 3",
        tags=["Loot", "Test"],
        entries=["1 thingy"],
        public=True
    )


    loottable_4 = LootTable(
        uuid.uuid4(),
        userId=uuid.uuid4(),
        name="Loot Table 4",
        tags=["Loot", "Test"],
        entries=["1 thingy"],
        public=True
    )

    return [loottable_1, loottable_2, loottable_3, loottable_4]


def test_loottable_list_without_parameters(domain_loottables):
    repo = mock.Mock()
    repo.list.return_value = domain_loottables

    loottable_list_use_case = uc.LootTableListUseCase(repo)
    request_object = req.LootTableListRequestObject.from_dict({})

    response_object = loottable_list_use_case.execute(request_object)
    print("Response: {}".format(response_object))
    assert bool(response_object) is True
    repo.list.assert_called_with(filters=None)

    assert response_object.value == domain_loottables


def test_loottable_list_with_filters(domain_loottables):
    repo = mock.Mock()
    repo.list.return_value = domain_loottables

    loottable_list_use_case = uc.LootTableListUseCase(repo)
    qry_filters = {'a': 5}
    request_object = req.LootTableListRequestObject.from_dict({'filters': qry_filters})

    response_object = loottable_list_use_case.execute(request_object)

    assert bool(response_object) is True
    repo.list.assert_called_with(filters=qry_filters)

    assert response_object.value == domain_loottables


def test_loottable_list_handles_generic_error():
    repo = mock.Mock()
    repo.list.side_effect = Exception('Just some error, you know')

    loottable_list_use_case = uc.LootTableListUseCase(repo)
    request_object = req.LootTableListRequestObject.from_dict({})

    response_object = loottable_list_use_case.execute(request_object)

    assert bool(response_object) is False
    assert response_object.value == {
        'type': res.ResponseFailure.SYSTEM_ERROR,
        'message': 'Exception: Just some error, you know'
    }

def test_loottable_list_handles_bad_request():
    repo = mock.Mock()

    loottable_list_use_case = uc.LootTableListUseCase(repo)
    request_object = req.LootTableListRequestObject.from_dict({'filters': 5})

    response_object = loottable_list_use_case.execute(request_object)

    assert bool(response_object) is False
    assert response_object.value == {
        'type': res.ResponseFailure.PARAMETERS_ERROR,
        'message': "filters: Is not iterable"
    }