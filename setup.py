from setuptools import setup

setup(
    name='loot_tables',
    version='0.1.0',
    description="A random table roller api on AWS lambda",
    author="Alexander P. Burgoon",
    author_email='apburgoon@gmail.com',
    packages=[
        'loot_tables',
    ],
    package_dir={'loot_tables':
                 'loot_tables'},
    include_package_data=True,
    license="MIT license",
    zip_safe=False,
    keywords='loot_tables',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests'
)