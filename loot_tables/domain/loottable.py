from loot_tables.shared .domain_model import DomainModel


class LootTable(DomainModel):


    def __init__(self, id, userId="", name: str="", tags: list=[], entries: list=[], public: bool=True):
        self.Id = id
        self.UserId = userId
        self.Name = name
        self.Tags = tags
        self.Entries = entries
        self.Public = public

    
    @classmethod
    def from_dict(cls, adict):
        return LootTable(adict['id'], userId=adict['userId'], name=adict['name'], tags=adict['tags'], entries=adict['entries'], public=adict['public'])

    def to_dict(self):
        return {
            'id': self.Id,
            'userId': self.UserId,
            'name': self.Name,
            'tags': self.Tags,
            'entries': self.Entries,
            'public': self.Public
        }


    def __eq__(self, other):
        return self.to_dict() == other.to_dict()