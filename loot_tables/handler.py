import os
import json

from loot_tables.repo.dynamodb_repo import DynamoDbLootTableRepo
from loot_tables.use_cases.loottable_use_cases import LootTableListUseCase
from loot_tables.use_cases.request_objects import LootTableListRequestObject
from loot_tables.serializers.loottable_serlializer import LootTableEncoder

stage = os.getenv('STAGE')
if stage == 'prod':
    from loot_tables.settings import ProdConfig as cfg
elif stage == 'test':
    from loot_tables.settings import TestConfig as cfg
else:
    from loot_tables.settings import DevConfig as cfg


def list_loot_tables(event, context):
    repo = DynamoDbLootTableRepo(client=cfg.CLIENT, table=cfg.TABLE)
    use_case = LootTableListUseCase(repo)
    request = LootTableListRequestObject(filters=event.get('queryStringParameters.filters', None))
    response = use_case.execute(request)
    jsonResp = json.dumps(response.value, cls=LootTableEncoder)
    return jsonResp