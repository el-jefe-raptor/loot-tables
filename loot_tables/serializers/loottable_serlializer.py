import json


class LootTableEncoder(json.JSONEncoder):

    def default(self, o):
        try:
            to_serialize = {
                'id': str(o.Id),
                'userId': str(o.UserId),
                'name': o.Name,
                'tags': o.Tags,
                'entries': o.Entries,
                'public': o.Public
            }
            return to_serialize
        except AttributeError:
            return super().default(o)