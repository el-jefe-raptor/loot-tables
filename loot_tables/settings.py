import os
import boto3


class Config(object):
    """Base configuration."""

    APP_DIR = os.path.abspath(os.path.dirname(__file__)) # this dir
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    TABLE = os.getenv('DYNAMODB_TABLE')
    REGION = os.getenv('REGION')

class ProdConfig(Config):
    """Production config"""
    ENV = 'prod'
    DEBUG = False
    CLIENT = boto3.resource('dynamodb', region_name=Config.REGION)


class DevConfig(Config):
    """Development config"""
    ENV = 'dev'
    DEBUG = True
    CLIENT = boto3.resource('dynamodb', region_name=Config.REGION)


class LocalConfig(Config):
    """Test config"""
    ENV = 'test'
    TESTING = True
    DEBUG = True
    CLIENT = boto3.resource('dynamodb', region_name=Config.REGION, endpoint_url="http://127.0.0.1:8000/")

class TestConfig(Config):
    """Test config"""
    ENV = 'test'
    TESTING = True
    DEBUG = True
    CLIENT = boto3.resource('dynamodb', region_name=Config.REGION, endpoint_url="http://127.0.0.1:8000/")