from loot_tables.domain import loottable as lt


class LootTableRepo(object):

    def list(self, filters=None):
        raise  NotImplementedError("process_request() not implemented by UseCase class")