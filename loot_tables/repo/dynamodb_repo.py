import boto3
from boto3.dynamodb.conditions import Key, Attr

from loot_tables.domain import loottable as lt
from loot_tables.repo import repo


class DynamoDbLootTableRepo(repo.LootTableRepo):

    def __init__(self, client: boto3.client = None, table=None):
        repo.LootTableRepo.__init__(self)

        if client is None:
            raise Exception("Missing client for {}".format(
                self.__class__.__name__))
        self.client = client

        if table is None:
            raise Exception("Missing Dynamodb table for {}".format(
                self.__class__.__name__))
        self.table = self.client.Table(table)

    def list(self, filters: dict = None):
        result = {'Items': []}

        if filters is None:
            result = self.table.scan()
        else:
            filter_exp = None
            for k, v in filters.items():
                if filter_exp is None:
                    filter_exp = Key(k).eq(v)
                else:
                    filter_exp = filter_exp & Key(k).eq(v)
            result = self.table.query(KeyConditionExpression=filter_exp)
        return result['Items']
