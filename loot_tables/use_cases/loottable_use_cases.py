from loot_tables.shared import use_case as uc
from loot_tables.shared import response_object as res

from loot_tables.shared import request_object as ro

class LootTableListUseCase(uc.UseCase):
    
    def __init__(self, repo):
        self.repo = repo

    def process_request(self, request_object: ro.RequestObject):
        domain_loottable = self.repo.list(filters=request_object.filters)
        return res.ResponseSuccess(domain_loottable)